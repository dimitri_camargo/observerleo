
package observer;


public class Main {


    public static void main(String[] args) {
        StockExchange Bovespa = new StockExchange();
        //Creating the dirst Observer
        DollarStockHolder obs1 = new DollarStockHolder(Bovespa);
        //Creating second Observer
        GoldStockHolder obs2 = new GoldStockHolder(Bovespa);
        //Today Information
        Bovespa.setStock(3.88, 151);
        //Third Observer
        DollarStockHolder obs3 = new DollarStockHolder(Bovespa);
        //Tomorrow Informations
        Bovespa.setStock(2, 85);
        //Thursday information
        Bovespa.setStock(3.60, 114);                                                                                                                                                                                                                                                                                                                                                   
        
    }
    
}
